exports.command = function (email, password) {
  this
    .url('http://dev.local.me:8181')
    .waitForElementVisible('body', 1000)
    .setValue('input[type=email]', email)
    .setValue('input[type=password', password)
    .waitForElementVisible('button[type=submit]', 1000)
    .click('button[type=submit]')

  return this;
};
