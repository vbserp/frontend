module.exports = {
  'User can view category list' : function (browser) {
    browser
      .login('fernando.basso@vbsmidia.com.br', '123456')
      .pause(1500)
      .click('.main-navigation-drawer a[href*=categories]')
      .pause(1500)
      .assert.containsText('body', 'Categorias - Listagem')
      .assert.containsText('body', 'VBS Mídia')
      .end();
  }
};
