module.exports = {
  'User can log into the system' : function (browser) {
    browser
      .login('fernando.basso@vbsmidia.com.br', '123456')
      .pause(1000)
      .assert.containsText('body', 'Desenvolvido por')
      .assert.containsText('body', 'VBS Mídia')
      .end();
  }
};
