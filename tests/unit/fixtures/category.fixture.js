const category1 = {
  id: 1,
  name: 'Foo Category',
};

const category2 = {
  id: 2,
  name: 'Bar Category',
};

const category3 = {
  id: 3,
  name: 'Linux Category',
};

const category4 = {
  id: 4,
  name: 'Open Source Category',
};

export {
  category1,
  category2,
  category3,
  category4,
};
