import { mount, shallow, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import CategoryIndex from '@cmps/CategoryIndex.vue';
import CategoryForm from '@cmps/CategoryForm.vue';
import axios from 'axios';
import { store } from '.@/store/store';

import * as H_ from '../helpers';

// Jest mocks are hoisted to the top of the file no mater
// where you put them.
// https://facebook.github.io/jest/docs/en/manual-mocks.html#using-with-es-module-imports
jest.mock('axios', () => ({
  // path is what comes from the parameter to axios.get('/some-path').
  get: jest.fn((path) => {
    if ('/categories/1' === path) {
      return Promise.resolve({
        status: 200,
        data: { id: 1, name: 'Foo' },
      });
    }
    else if ('/category-404' === path) {
      return Promise.resolve({
        status: 404,
        data: { error: 'Not found: 404' }
      });
    }
    else {
      return Promise.reject({ error: 'Unknown Error' });
    }
  }),

  post: jest.fn((path, categoryPayload) => {
    // POST _without_ and ID means we are creating a new resource.
    if (path === '/categories') {
      if (0 < categoryPayload.name.length) {
        return Promise.resolve({
          status: 201,
          data: { id: categoryPayload.id, name: categoryPayload.name },
        });
      }
      // Simulate user provided invalid or insufficient data.
      else if (0 === categoryPayload.name.length) {
        return Promise.resolve({
          status: 422, // Unprocessable Entity
          statusText: 'Unprocessable Entity',
          data: { id: categoryPayload.id, name: categoryPayload.name },
        });
      }
    }
  }),

  patch: jest.fn((path, payload) => {

    // PATCH with an ID means we are updating an existing resource.
    if (path === '/categories/1') {
      if (0 < payload.name.length) {
        return Promise.resolve({
          status: 200,
          data: { id: payload.id, name: payload.name },
        });
      }
      else if (0 === payload.name.length) {
        return Promise.resolve({
          status: 422,
          statusText: 'Unprocessable Entity',
          data: { id: payload.id, name: payload.name },
        });
      }
      else {
        return Promise.reject({ error: 'Unknown Error Updating Category' });
      }
    }
  }),
}));

describe('CategoryForm.spec.js', function () {
  let wrpNew;
  let wrpEdit;

  const routeIndex = { path: '/categories', name: 'categories' };
  const routeNew = { path: '/categories/new', name: 'category-new' };
  const routeEdit = { path: '/categories/:category_id/edit', name: 'category-edit' };
  const routes = [ routeIndex, routeNew, routeEdit ];
  const routerNew = new VueRouter({ mode: 'abstract', routes });
  const routerEdit = new VueRouter({ mode: 'abstract', routes });

  beforeEach(() => {

    const localVueNew = createLocalVue();
    const localVueEdit = createLocalVue();

    localVueNew.use(VueRouter);
    localVueNew.use(Vuetify);

    localVueEdit.use(VueRouter);
    localVueEdit.use(Vuetify);

    routerNew.push({ name: 'category-new' });
    routerEdit.push({ name: 'category-edit', params: { category_id: 1 }});

    wrpNew = mount(CategoryForm, {
      localVue: localVueNew,
      router: routerNew,
    });

    wrpEdit = mount(CategoryForm, {
      localVue: localVueEdit,
      router: routerEdit,
    });
  });

  describe('Default Structure', () => { // {{{2

    it('has container with proper <entity> and <entity>-form css classes', () => {
      expect(wrpNew.contains('.category.category-form')).toBe(true);
      expect(wrpEdit.contains('.category.category-form')).toBe(true);
    });

    it('has ‘isEditing’ true or false', () => {
      expect(wrpNew.vm.isEditing).toBe(false);
      expect(wrpEdit.vm.isEditing).toBe(true);
    });

    it('has ‘h2.crud-title’ with proper create/edit text', () => {
      expect(wrpNew.contains('h2.crud-title')).toBe(true);
      expect(wrpEdit.contains('h2.crud-title')).toBe(true);
      expect(wrpNew.find('h2.crud-title').text()).toEqual('Categorias - Adicionar');
      expect(wrpEdit.find('h2.crud-title').text()).toEqual('Categorias - Editar');
    });

    it('has a ‘form’ for the new/editing <entity>', () => {
      expect(wrpNew.contains('.category-form form')).toBe(true);
      expect(wrpEdit.contains('.category-form form')).toBe(true);
    });

    it('has cancel and submit buttons', () => {
      expect(wrpNew.contains('.category-form .btn-cancel')).toBe(true);
      expect(wrpNew.contains('.category-form .btn-submit')).toBe(true);
      expect(wrpEdit.contains('.category-form .btn-cancel')).toBe(true);
      expect(wrpEdit.contains('.category-form .btn-submit')).toBe(true);
    });

    it('has container with proper class plus label for each field', () => {
      const elemNew = wrpNew.find('.wrp-category-name');
      const elemEdit = wrpEdit.find('.wrp-category-name');

      expect(elemNew.find('label').text()).toEqual('Nome da Categoria');
      expect(elemEdit.find('label').text()).toEqual('Nome da Categoria');

      expect(elemNew.contains("input[name='category[name]']")).toBe(true);
      expect(elemEdit.contains("input[name='category[name]']")).toBe(true);
    });

  }); // Default Structure }}}

  describe('Form - Create/Edit', () => { // {{{2

    it('new <entity> form empty fields', () => {
      const elemNew = wrpNew.find('.wrp-category-name');
      expect(elemNew.find("input[name='category[name]']").element.value).toEqual('');
    });

    it('edit <entity> form has filled in fields', () => {
      const elemEdit = wrpEdit.find('.wrp-category-name');
      expect(elemEdit.find("input[name='category[name]']").element.value).toEqual('Foo');
    });

    it('cancels the create/edit action', () => {
      const spyNew = jest.spyOn(wrpNew.vm.$router, 'push');
      const spyEdit = jest.spyOn(wrpEdit.vm.$router, 'push');
      wrpNew.find('.category-form .btn-cancel').trigger('click');
      wrpEdit.find('.category-form .btn-cancel').trigger('click');
      const routeArgs = {
        name: 'categories',
        params: {},
        path: '/categories',
      };
      expect(spyNew).toHaveBeenCalledWith(routeArgs);
      expect(spyEdit).toHaveBeenCalledWith(routeArgs);
    });

    it('submits create form', () => {
      const spy = jest.spyOn(wrpNew.vm, 'requestCreate');
      const btn = wrpNew.find('button.btn-submit');

      typeIn(wrpNew, '.wrp-category-name input', 'New Category');
      btn.trigger('click');
      expect(spy).toHaveBeenCalled();
    });

    it('submits edit form', () => {
      const spy = jest.spyOn(wrpEdit.vm, 'requestUpdate');
      const btn = wrpEdit.find('button.btn-submit');

      typeIn(wrpEdit, '.wrp-category-name input', 'Edited Category');
      btn.trigger('click');

      expect(wrpEdit.vm.category.name).toEqual('Edited Category');
      expect(spy).toHaveBeenCalled();
    });

    it('redirects to index if create was successfull', async () => {
      const pushSpy = jest.spyOn(wrpNew.vm.$router, 'push');
      const btnSubmit = wrpNew.find('button.btn-submit');

      typeIn(wrpNew, "input[name='category[name]']", 'New Category');
      btnSubmit.trigger('click');

      await wrpNew.vm.$nextTick();

      expect(pushSpy).toHaveBeenCalledWith({
        name: 'categories',
        params: {},
        path: '/categories'
      });
    });

    it('redirects to index if update was successful', async () => {
      const spyPush = jest.spyOn(wrpEdit.vm.$router, 'push');
      const btn = wrpEdit.find('button.btn-submit');

      expect(wrpEdit.vm.$route.name).toEqual('category-edit');
      expect(wrpEdit.vm.category.name).toEqual('Foo');

      typeIn(wrpEdit, '.wrp-category-name input', 'Edited category');
      expect(wrpEdit.vm.category.name).toEqual('Edited category');

      btn.trigger('click');

      await wrpEdit.vm.$nextTick();

      expect(spyPush).toHaveBeenCalledWith({
        name: 'categories',
        params: {},
        path: '/categories'
      });

      // TODO:
      // $route.name now is still _not_ 'categories'. Not even if we
      // wait for next tick. Why not‽
    });

    it('updates store with alert message when create fails', async () => {
      // @TODO: Change 200 to 201.
      // Component detectes a non 200 category response and updates
      // store so the UI app it displays an alert/message box
      // telling the user about the problem.

      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpNew.find('.btn-submit');
      const inputName = wrpNew.find('.wrp-category-name input');

      // Name can't be blank, so it fails and we can test it!
      typeIn(wrpNew, inputName, '');
      btnSubmit.trigger('click');

      await wrpNew.vm.$nextTick();

      expect(spyAlert).toHaveBeenCalledWith({
        type: 'warning',
        text: 'Erro ao inserir dados da categoria.'
      });
    });

    it('updates store with alert message when create succeeds', () => {
      // Component detectes a non 200 category response and updates
      // store so the UI app it displays an alert/message box
      // telling the user about the problem.

      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpNew.find('.btn-submit');
      const inputName = wrpNew.find('.wrp-category-name input');

      // Name can't be blank, so it fails.
      typeIn(wrpNew, inputName, 'My New category');
      btnSubmit.trigger('click');

      return wrpNew.vm.$nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'success',
            text: 'Dados da categoria inseridos com sucesso!',
          });
        });
    });

    it('updates store with alert message when create succeeds', () => {
      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpNew.find('.btn-submit');
      const inputName = wrpNew.find('.wrp-category-name input');

      typeIn(wrpNew, inputName, 'My New Category');
      btnSubmit.trigger('click');

      return Vue.nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'success',
            text: 'Dados da categoria inseridos com sucesso!',
          });
        });
    });

    it('updates store with alert message when update fails', () => {

      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpEdit.find('.btn-submit');
      const inputName = wrpEdit.find('.wrp-category-name input');

      typeIn(wrpEdit, inputName, '');
      btnSubmit.trigger('click');

      return Vue.nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'warning',
            text: 'Erro ao atulizar dados da categoria.'
          });
        });
    });

    it('updates store with alert message when update succeeds', () => {
      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpEdit.find('.btn-submit');
      const inputName = wrpEdit.find('.wrp-category-name input');

      typeIn(wrpEdit, inputName, 'Edited category');
      btnSubmit.trigger('click');

      return Vue.nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'success',
            text: 'Dados da categoria atualizados com sucesso!',
          });
        });
    });
  }); // Form - Create/Edit




});

