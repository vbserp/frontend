import { shallow, mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import CategoryIndex from '@cmps/CategoryIndex.vue';
import CategoryForm from '@cmps/CategoryForm.vue';
import axios from 'axios';
import { store } from '@/store/store';

import { category1, category2 } from '@fixtures/category.fixture.js';


// Jest mocks are hoisted to the top of the file no mater
// where you put them.
// https://facebook.github.io/jest/docs/en/manual-mocks.html#using-with-es-module-imports
jest.mock('axios', () => ({
  // path is what comes from the parameter to axios.get('/some-path').
  get: jest.fn((path) => {
    // Can't use `import` here and jest mocks cannot access variables
    // imported above, outside jest.mock() scope.
    const categories = require('@fixtures/category.fixture.js');
    if ('/categories' === path) {
      return Promise.resolve({
        status: 200,
        data: [categories.category1, categories.category2]
      });
    }
    else if ('/categories-404' === path) {
      return Promise.reject({
        status: 404,
        data: { error: 'Not Found: 404' },
      });
    }
    else {
      return Promise.reject({
        status: 500,
        data: { error: 'Internal Server Error: 500' },
      });
    }
  }),
}));

describe('CategoryIndex.spec.js', function () {
  let wrp;

  const routes = [
    { path: '/categories', name: 'categories' },
    { path: '/categories/new', name: 'category-new' },
    { path: '/categories/:category_id/edit', name: 'category-edit' }
  ];

  const router = new VueRouter({routes});

  beforeEach(() => {

    const localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuetify);

    router.push({ name: 'categories' });

    wrp = mount(CategoryIndex, {
      localVue: localVue,
      router,
    });
  });


  describe('Basic Structure', () => {

    it('has a mounted hook', () => {
      expect(typeof CategoryIndex.mounted).toBe('function');
    });

    it('has correct route', () => {
      expect(wrp.vm.$route.name).toEqual('categories');
    });

    it('has container with proper <entity> and <entity>-index css classes', () => {
      expect(wrp.classes()).toContain('category');
      expect(wrp.classes()).toContain('category-index');
    });

    it('has an h2 tag with proper class and text', () => {
      expect(wrp.find('h2.crud-title').text()).toEqual('Categorias - Listagem');
    });

    it('has add btn', () => {
      expect(wrp.find('.btn-add-category').exists()).toBe(true);
    });

    it('always displays .index-of-things', () => {
      // Displays it no matter whether or not we have people to display.
      wrp.setData({ categories: [] });
      expect(wrp.contains('.index-of-things')).toBe(true);

      wrp.setData({ people: [category1, category2]});
      expect(wrp.contains('.index-of-things')).toBe(true);
    });

    it('conditionally displays container <entity>-index-row', () => {
      // If categories array is empty, the v-for has nothing to iterate
      // over and we therefore render no rows on the UI.
      wrp.setData({ categories: []});
      expect(wrp.contains('.category-index-row')).toBe(false);

      // But if we have categories to show, we then should see the
      // exact number of rows.
      // @TODO: (ignoring pagination for this stage of the project).
      wrp.setData({ categories: [category1, category2] });
      expect(wrp.findAll('.category-index-row').length).toEqual(2);
    });

    it('each <entity>-index-row has an edit button', () => {
      wrp.setData({ categories: [category1, category2] });
      const rows = wrp.findAll('.category-index-row');
      // <v-btn> renders as <a href="..."> because of :to property.
      expect(rows.contains('a.btn-edit')).toBe(true);
    });
  }); // Basic Structure


  describe('Fetch/Persist Data', () => {
    it('fetches and renders some <entities>', async () => {
      expect.assertions(2);
      await wrp.vm.fetchCategories();
      wrp.update();
      const rows = wrp.findAll('.index-of-things .category-index-row');
      expect(rows.at(0).text()).toContain('Foo');
      expect(rows.at(1).text()).toContain('Bar');
    });

    it('handles requests to <entities> index 404 url', async () => {
      await wrp.vm.fetchCategories('/categories-404').catch(err => {
        expect(err.status).toBe(404);
        expect(err.data.error).toBe('Not Found: 404');
        store.displayAlertAction({
          type: 'warning',
          text: 'Erro a listar categorias.',
        });

        expect(store.state.alert.type).toEqual('warning');
        expect(store.state.alert.text).toEqual('Erro a listar categorias.');
        expect(store.state.alert.display).toBe(true);
      });
    });

    it('invokes fetch<entities> when component is created', () => {
      const fetchCategories = jest.fn();
      const lwrp = shallow(CategoryIndex, {
        methods: { fetchCategories }
      });
      expect(fetchCategories).toHaveBeenCalled();
    });

    it('invokes fetch<entities> to a 500 sorts of serve response', async () => {
      expect.assertions(1);
      return wrp.vm.fetchCategories('/no-no-no')
        .catch(err => {
          expect(err.data.error).toEqual('Internal Server Error: 500');
        });
    });
  }); // Fetch/Persist Data
  
});

